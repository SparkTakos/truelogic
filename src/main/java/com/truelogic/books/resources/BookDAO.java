package com.truelogic.books.resources;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.truelogic.books.api.Bookshelf;
import com.truelogic.jooq.db.tables.Author;
import com.truelogic.jooq.db.tables.Book;
//import com.truelogic.db.tables.*;
//import com.truelogic.books.resources.Bookshelf;
import com.truelogic.jooq.db.tables.BookAuthor;
import com.truelogic.jooq.db.tables.Publisher;

public class BookDAO {
	
	Connection connection = null;
	
	private Connection connectDB(){
		String userName = "root";
	    String password = "mysql";
	    String url = "jdbc:mysql://localhost:3306/books";
	
	    // Connection is the only JDBC resource that we need
	    // PreparedStatement and ResultSet are handled by jOOQ, internally
	    try {
	    	Class.forName("com.mysql.jdbc.Driver");
	    	connection = DriverManager.getConnection(url, userName, password);
	    
	        System.out.println("CONECTADO OK!");
	    } 
	    catch (Exception e) {
	    	System.out.println("CONECTADO NO OK!");
	        e.printStackTrace();
	    }	    
	    return connection;
	}
	
	public Map<Integer, Bookshelf> getBooks(String title, String description){
		//List<Bookshelf> booksList = new ArrayList<Bookshelf>();
		Map<Integer,Bookshelf> books = new HashMap<Integer, Bookshelf>(); 
		
			
		System.out.println("IN description: " + description);	    
	    System.out.println("IN title: " + title);
		
		connection = connectDB();
		
		DSLContext create = DSL.using(connection, SQLDialect.MYSQL);
		//Result<Record> result = create.select().from(Book.BOOK).fetch();
		Result<Record> result = create.select()
				.from(BookAuthor.BOOK_AUTHOR
				.join(Book.BOOK)
				.on(Book.BOOK.BOOK_ID.eq(BookAuthor.BOOK_AUTHOR.BOOK_ID))
				.join(Author.AUTHOR)
				.on(Author.AUTHOR.AUTHOR_ID.eq(BookAuthor.BOOK_AUTHOR.AUTHOR_ID))
				.join(Publisher.PUBLISHER)
				.on(Book.BOOK.PUBLISHER_ID.eq(Publisher.PUBLISHER.PUBLISHER_ID))
				.and(Book.BOOK.TITLE.like("%" + title + "%")).or(Book.BOOK.DESCRIPTION.like("%" + description + "%")))
				.fetch();
		
		
		
		List<String> authors = new ArrayList<String>();
		for (Record r : result) {
		    Integer id = r.getValue(BookAuthor.BOOK_AUTHOR.BOOK_ID);		    
		    String titleR = r.getValue(Book.BOOK.TITLE);
		    String isbn = r.getValue(Book.BOOK.ISBN);
		    String subtitle = r.getValue(Book.BOOK.SUBTITLE);
		    String published = r.getValue(Book.BOOK.PUBLISHED).toString();
		    Integer pages = r.getValue(Book.BOOK.PAGES);		    
		    String publisherName = r.getValue(Publisher.PUBLISHER.NAME);
		    String descriptionR = r.getValue(Book.BOOK.DESCRIPTION);
		    
		    
		    String authorName = r.getValue(Author.AUTHOR.NAME);
		    //String lastName = r.getValue(AUTHOR.LAST_NAME);

		    System.out.println("id: " + id);		    
		    System.out.println("titleR: " + titleR);
		    System.out.println("isbn: " + isbn);
		    System.out.println("subtitle: " + subtitle);
		    System.out.println("published: " + published);
		    System.out.println("pages: " + pages);		    
		    System.out.println("publisherName: " + publisherName);
		    System.out.println("descriptionR: " + descriptionR);
		    
		    System.out.println("authorName: " + authorName);
		    authors.add(authorName);
		    
		    if (!books.containsKey(id)){
		    	Bookshelf bookshelf = new Bookshelf();
			    bookshelf.setId(id);
			    bookshelf.setAuthors(authors);
			    bookshelf.setDescription(description);
			    bookshelf.setIsbn(isbn);
			    bookshelf.setPages(pages);
			    bookshelf.setPublished(published);
			    bookshelf.setPublisher(publisherName);
			    bookshelf.setDescription(descriptionR);
			    bookshelf.setSubtitle(subtitle);
			    bookshelf.setTitle(titleR);
			    bookshelf.setAuthors(authors);
			    books.put(id, bookshelf);
		    }
		}
		
		try{
			if (connection != null) connection.close();
			System.out.println("Conexion cerrada");
		}
		catch(SQLException sqlException){
			System.out.println("Error sqlException:" + sqlException.getMessage());
			sqlException.printStackTrace();
		}
		
		return books;
	}
}