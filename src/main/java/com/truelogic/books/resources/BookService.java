package com.truelogic.books.resources;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.truelogic.books.api.Bookshelf;
//import com.truelogic.dao.BookDAO;


//import com.soniko.test.api.Event;

@Path("events")
@Produces(MediaType.APPLICATION_JSON)
public class BookService {
	@GET
	public Response getMsg(@QueryParam("title") String title, @QueryParam("description") String description) {
    //public List<Bookshelf> getBooks() {
        /*Event e = new Event();
        e.setDate(new Date());
        e.setName("Birthday");
        e.setId(1L);
        e.setDescription("Please be on time!");
        e.setLocation("221B Baker Street");
        return Collections.singletonList(e);*/
		//return null;
		System.out.println("Init...");
		String output = "Title : " + title;
		output += " description : " + description;
		BookDAO bookDAO = new BookDAO();
		Map<Integer, Bookshelf> books = bookDAO.getBooks(title, description);
		
		System.out.println("End.");
		List<Bookshelf> booksList = new ArrayList<Bookshelf>(books.values());
		return Response.status(200).entity(booksList).build();
    }
}
