package com.truelogic.books;

//import com.soniko.test.resources.EventResource;
import com.truelogic.books.resources.BookService;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class DW_BooksAPIApplication extends Application<DW_BooksAPIConfiguration> {

    public static void main(final String[] args) throws Exception {
        new DW_BooksAPIApplication().run(args);
    }

    @Override
    public String getName() {
        return "DW_BooksAPI";
    }

    @Override
    public void initialize(final Bootstrap<DW_BooksAPIConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final DW_BooksAPIConfiguration configuration,
                    final Environment environment) {
        // TODO: implement application
    	/*EventResource eventResource = new EventResource();
        environment.jersey().register(eventResource);*/
        
        BookService bookService = new BookService();
        environment.jersey().register(bookService);
    }

}
